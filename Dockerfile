FROM alpine:latest
MAINTAINER Vitor Brito <main@vitorbrito.com>

ENV PERCONA_TOOLKIT_VERSION 3.0.5

RUN set -x &&  apk add --update perl perl-dbi perl-dbd-mysql perl-io-socket-ssl perl-term-readkey make ca-certificates wget

RUN update-ca-certificates

RUN  wget -O /tmp/percona-toolkit.tar.gz https://www.percona.com/downloads/percona-toolkit/${PERCONA_TOOLKIT_VERSION}/source/tarball/percona-toolkit-${PERCONA_TOOLKIT_VERSION}.tar.gz

RUN tar -xzvf /tmp/percona-toolkit.tar.gz -C /tmp

WORKDIR /tmp/percona-toolkit-${PERCONA_TOOLKIT_VERSION}
RUN  perl Makefile.PL

RUN  make &&  make test &&  make install

RUN apk del make ca-certificates wget  && rm -rf /var/cache/apk/* /tmp/percona-toolkit*
